<?php 

class GenreQuery {

	public function __construct($pdo)
	{
		$this->pdo = $pdo;
	}

	public function getAll()
	{
		$sql = 'SELECT * FROM genres';
		$statement = $this->pdo->prepare($sql);
		$statement->execute();
		$genres = $statement->fetchAll();

		return $genres;
	}


}