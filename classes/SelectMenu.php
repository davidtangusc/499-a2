<?php 

class SelectMenu {
	protected $data;
	protected $name;

	public function __construct($name, $data)
	{
		$this->name = $name;
		$this->data = $data;
	}

	protected function buildSelectTag()
	{
		$name = $this->name;
		return "<select name='$name'>";
	}
}