<?php 

class Song {
	protected $pdo;
	protected $id;
	protected $title;
	protected $artist;
	protected $genre;
	protected $price;

	public function __construct($pdo)
	{
		$this->pdo = $pdo;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setArtistId($artistId)
	{
		$this->artist = $artistId;
	}

	public function setGenreId($genreId)
	{
		$this->genre = $genreId;
	}

	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getId()
	{
		return $this->id;
	}

	public function save()
	{
		$statement = $this->pdo->prepare($this->getSql());
		$statement->bindParam(1, $this->title);
		$statement->bindParam(2, $this->artist);
		$statement->bindParam(3, $this->genre);
		$statement->bindParam(4, $this->price);
		$response = $statement->execute();

		$this->id = $this->pdo->lastInsertId();

		return $response;
	}

	protected function getSql()
	{
		return "
			INSERT INTO songs (title, artist_id, genre_id, price)
			VALUES (?, ?, ?, ?)
		";
	}
}