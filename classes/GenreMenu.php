<?php 

require_once 'SelectMenu.php';

class GenreMenu extends SelectMenu {

	public function __toString()
	{
		$html = $this->buildSelectTag();
		
		foreach($this->data as $genre)
		{
			$id = $genre['id'];
			$html .= "<option value='$id'>" . $genre['genre'] . '</option>';
		}

		return $html . '</select>';
	}

}