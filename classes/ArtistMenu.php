<?php 

require 'SelectMenu.php';

class ArtistMenu extends SelectMenu {

	public function __toString()
	{
		$html = $this->buildSelectTag();
		
		foreach($this->data as $artist)
		{
			$id = $artist['id'];
			$html .= "<option value='$id'>" . $artist['artist_name'] . '</option>';
		}

		return $html . '</select>';
	}


}